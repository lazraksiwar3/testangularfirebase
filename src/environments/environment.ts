// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase : {
      apiKey: "AIzaSyCFuQPUiM83bcRYp1jfRh224WCS8fc9TF0",
      authDomain: "testangular-67a13.firebaseapp.com",
      databaseURL: "https://testangular-67a13.firebaseio.com",
      projectId: "testangular-67a13",
      storageBucket: "testangular-67a13.appspot.com",
      messagingSenderId: "822693173759",
      appId: "1:822693173759:web:e1f70fc32b98fc0ac3efca",
      measurementId: "G-YCLCZ8XDMH"
  }

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
