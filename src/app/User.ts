export class User {
    key: string;
    nom: string;
    prenom: string;
    dateNaiss: Date;
    email: string;
    tel: string;
    active = true;
}
