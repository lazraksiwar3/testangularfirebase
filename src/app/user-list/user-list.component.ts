import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  users: any;
  constructor(private userService: UserService) { }

  ngOnInit() {
    this.getUsersList();
  }
  getUsersList() {
    this.userService.getUserList().snapshotChanges().pipe(
      map(changes =>
          changes.map(c =>
            ({ key: c.payload.key, ...c.payload.val() })
            )
            )
    ).subscribe(users => {
      this.users = users;
    });
  }
  deleteUsers() {
    this.userService.deleteAll().catch(err => console.log(err));
  }
}
