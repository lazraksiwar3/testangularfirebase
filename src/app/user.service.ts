import { Injectable } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AngularFireList, AngularFireDatabase } from '@angular/fire/database';
import { User } from './User';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private dbUrl = '/users';
  userRef: AngularFireList<User> = null;

  constructor( private db: AngularFireDatabase) {
    this.userRef = db.list(this.dbUrl)
   }
   createUser(user: User): void {
    this.userRef.push(user);
  }
 
  updateUser(key: string, value: any): Promise<void> {
    return this.userRef.update(key, value);
  }
 
  deleteUser(key: string): Promise<void> {
    return this.userRef.remove(key);
  }
 
  getUserList(): AngularFireList<User> {
    return this.userRef;
  }
 
  deleteAll(): Promise<void> {
    return this.userRef.remove();
  }
}
