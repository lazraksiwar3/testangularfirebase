import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { User } from '../User';
import {Router} from "@angular/router"

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  user: User = new User();
  submitted = false;

  constructor( private userService: UserService, private router: Router) { }

  ngOnInit() {
  }
  newUser(): void {
    this.submitted = false;
    this.user = new User();
  }
  save() {
    this.userService.createUser(this.user);
    this.user = new User();
  }
  onSubmit() {
    this.submitted = true;
    this.save();
    this.router.navigate(['/users'])
  }


}
