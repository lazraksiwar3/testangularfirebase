import { Component, OnInit, Input } from '@angular/core';
import { User } from '../User';
import { UserService } from '../user.service';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {
  @Input() user: User;
  constructor(private userService: UserService ) { }

  ngOnInit() {
  }
  updateActive(isActive: boolean) {
    this.userService
      .updateUser(this.user.key, { active: isActive })
      .catch(err => console.log(err));
  }
  deleteUser() {
    this.userService
      .deleteUser(this.user.key)
      .catch(err => console.log(err));
  }

}
